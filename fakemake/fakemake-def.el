;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'org-development-lisp
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("org-development-lisp-core"
                       "org-development-lisp-mode"
                       "org-development-lisp")
  test-phase-enabled nil
  site-lisp-config-prefix "50"
  license "GPL-3")
