;;;; lisp-system-name.asd

;; (cl:eval-when (:load-toplevel :execute)
;;    #+quicklisp (ql:quickload :literate-lisp)
;;    #-quicklisp (asdf:load-system :literate-lisp))

(asdf:defsystem #:lisp-system-name
  :description "No description."
  :author "author-string"
  :license "Unknown"
  ;; :version "20200214"
  :serial t
  :depends-on (#:literate-lisp)

  :around-compile (lambda (next)
                    (proclaim '(optimize 
				(compilation-speed 0)
                                (debug 3)
                                (safety 3)
                                (space 0)
                                (speed 0)))
                    (funcall next))

  :components ((:org "packages")
	       (:org "lisp-system-name")))
