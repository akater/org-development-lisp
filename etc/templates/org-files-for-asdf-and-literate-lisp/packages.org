# -*- coding: utf-8; mode: org-development-lisp; -*-
#+title: A lisp-system-name system's “package file”
#+subtitle: part of the =lisp-system-name= Common Lisp system
#+property: header-args :package common-lisp
#+author: =#<PERSON "akater" A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=

* lisp-system-name
#+begin_src lisp :tangle yes :results none
(cl:defpackage #:lisp-system-name
  (:use #:cl))
#+end_src
